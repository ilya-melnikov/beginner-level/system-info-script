package main

import (
	"fmt"
	"log"
	"os"
	"os/user"
	"runtime"
)

type SysInfo struct {
	Platform string ""
	Hostname string ""
	CPU      string ""
	GPU      string ""
	RAM      string ""
	Disk     string ""
}

func main() {
	si := SysInfo{
		Platform: runtime.GOOS,
	}

	var err error
	si.Hostname, err = os.Hostname()
	if err != nil {
		log.Fatal(fmt.Sprintf("failed to get hostname: %v", err))
	}

	if si.Platform == "windows" {
		processWindowsInfo(&si)
	} else if si.Platform == "darwin" {
		processDarwinInfo(&si)
	} else if si.Platform == "linux" {
		processLinuxInfo(&si)
	} else {
		log.Fatal(fmt.Sprintf("%s OS is not supported"), si.Platform)
	}

	fmt.Printf(`
		System information:
		Platform: %s,
		Hostname: %s
		
		`,
		si.Platform,
		si.Hostname,
	)
}

func processDarwinInfo(si *SysInfo) {
	//todo
}

func processWindowsInfo(si *SysInfo) {
	//todo
}

func processLinuxInfo(si *SysInfo) {
	current, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}

	if current.Uid != "0" {
		log.Fatal("requires superuser privilege")
	}
	//todo
}
